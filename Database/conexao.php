<?php

/**

Class for connection to the database. Currently configured for MySQL.
This class adopts the singleton design pattern. Therefore, one must access the method, statically getInstance to obtain an instance of the class in question.

Ex: Conexao::getInstance();

@author:  Raí Moreira
@version: 1.0
 */

class Conexao
{

    /*type database. Default mysql */

    private $typeDatabase = 'mysql';

    /**
    Location where the database is located.
    By default, it is set to 127.0.0.1 - IP that points to localhost.

     */
    private $urlDB = "localhost";

    /**

    User for database access. By default, it comes as root.

     */
    private $user = "root";

    /**

    Password to access the database. By default, it comes as a blank password.

     */

    private $password = "";

    /**

    Name of the database to which you want to connect.

     */
    private $nameDB = "hcpsystem";

    /**


    Port provided by the database for access

     */
    private $port = "3306";

    /**Do not modify**/
    public static $instance = null;

    /**Do not modify**/
    private $connection = null;

    /**
    The constructor is hidden because the singleton project pattern is applied
     */
    private function __construct()
    {}

    /**

    Obtain a connection with the Connection class.

     */

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Conexao;
        }

        return self::$instance;
    }

    /*
    It makes a connection to the database, using the information described in the attributes described above.

    Error: If the connection is not successful, the error is returned which caused problems.

     **/

    public function connect()
    {return $this->executeConnection();}

    private function executeConnection()
    {
        switch ($this->typeDatabase) {
            case 'mysql':
                return $this->conexaoMySQL();
            case 'postgresql':
                return $this->conexaoPostgresql();
            default:
                echo "Database type not found!";
                exit;
        }
    }

    private function conexaoMySQL()
    {
        try {
            $this->connection = mysqli_connect($this->urlDB, $this->user, $this->password, $this->nameDB, $this->port);

            if (!$this->connection) {
                echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
                exit;
            }

            return $this->connection;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function conexaoPostgresql()
    {
        try {
            $parameter_connection = "host=" . $this->urlDB . " port=" . $this->port . " dbname=" . $this->nameDB . " user=" . $this->user . " password=" . $this->password;
            $this->connection = pg_connect($parameter_connection);

            if (!$this->connection) {
                echo "Debugging error: " . pg_last_error() . PHP_EOL;
                exit;
            }

            return $this->connection;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
    Terminates an open connection to the database
     */

    public function close()
    {
        if ($this->connection != null) {
            mysqli_close($this->connection);
        }

    }

    public function typeDatabase()
    {return $this->typeDatabase;}

}
