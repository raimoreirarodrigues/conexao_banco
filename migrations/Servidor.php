<?php

require_once('Migration.php');
require_once('TypesData.php');

class Servidor extends Migration{

	public static function generateTable(){
		if(!Servidor::hasTable('servidores')){
			$collumns = new TypesData;
			$collumns->increments('id_cliente');
			$collumns->string('nome',100);
			Servidor::createTable('servidores',$collumns);
		}
	}
}

?>