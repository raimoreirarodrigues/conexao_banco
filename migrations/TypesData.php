<?php

class TypesData{

	protected $columns = [];

	public function increments($column)
    {
        $this->addColumn('INTEGER', $column, ['AUTO_INCREMENT', 'UNSIGNED ','PRIMARY KEY']);
        return $this;
    }

	public function string($column, $length = 255,$nullable = false){
        $this->addColumn('VARCHAR('.$length.')',$column,$length,$nullable);
        return  $this;
    }

	public function addColumn($type, $name, $parameters = [],$nullable=false){
        if(!is_array($parameters)){
          $parameters_column[] = $parameters;   
        }else{
            $parameters_column = $parameters;
        }
        $extras='';
        foreach ($parameters_column as $param) {
            $extras.=' '.$param;
        }
        $nullable = $nullable==true?' NULL ':'';
        $this->columns[] = $name.' '.$type.''.$extras.$nullable;
        
    }

}


?>