<?php

namespace App;

require_once ('Model.php');

class Banco extends Model{

 	protected $table   		= 'bancos';
	protected $timestamps  	= true;
	protected $softdelete 	= true;
	protected $primaryKey 	= 'id_banco';
 	protected $fillable 	= ['cd_banco','nn_banco','id_tenant'];

 	public function teste(){
 		return $this->hasOne(Cargo::class,'id_cargo','id_fornecedor');
 	}

 	public function teste2(){
 		return $this->hasOne(Cargo::class,'id_testando');
 	}

}


?>