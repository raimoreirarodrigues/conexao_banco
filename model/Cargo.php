<?php

namespace App;

require_once ('Model.php');

class Cargo extends Model{

 	public $table   		= 'cargos';
	protected $timestamps  	= true;
	protected $softdelete 	= true;
	protected $primaryKey 	= 'id_cargo';
 	protected $fillable 	= ['ds_cargo'];

}


?>