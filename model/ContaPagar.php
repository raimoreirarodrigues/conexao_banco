<?php

require_once ('Model.php');

class ContaPagar extends Model{

 	public $table   		= 'contas_pagar';
	protected $timestamps  	= true;
	protected $softdelete 	= true;
	protected $primaryKey 	= 'id_conta_pagar';
 	protected $fillable 	= [];

 	public function fornecedor(){
 		return $this->hasOne(Fornecedor::class,'id_fornecedor','id_fornecedor');
 	}

}


?>