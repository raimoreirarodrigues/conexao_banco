<?php

require_once ('Model.php');

class Departamento extends Model{

 	public $table   		= 'departamentos';
	protected $timestamps  	= true;
	protected $softdelete 	= true;
	protected $primaryKey 	= 'id';
 	protected $fillable 	= ['nome','email'];

}

?>