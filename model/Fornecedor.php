<?php

require_once ('Model.php');

class Fornecedor extends Model{

 	public $table   		= 'fornecedores';
	protected $timestamps  	= true;
	protected $softdelete 	= true;
	protected $primaryKey 	= 'id_fornecedor';
 	protected $fillable 	= [];

 	public function contas(){
 		return $this->hasMany(ContaPagar::class,'id_fornecedor');
 	}

}


?>