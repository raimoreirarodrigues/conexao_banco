<?php

include_once '../database/conexao.php';

/**

Class for help with manipulation with the database. Models that inherit this class will enjoy pre-deployed methods for performing common actions on the database, such as selecting data, entering new data, updating data, retrieving specific data, and erasing data.
The purpose is to minimize the redundancy of SQL statements, making it more practical to use the database.

@author:  Raí Moreira
@version: 1.0

 */

abstract class Model
{

    /*
    The name of the relationship registered in the database.

     **/

    private static $table;

    /*
    Name of the primary key of the relation.

     **/

    private static $primary_key;

    /*
    All the columns of the relation, except for the primary key, separated by another attribute, in this case, $primary_key

     **/

    private static $fillable;

    /*
    Checks whether the relation has the timestamp, that is, the fields created in and updated in

     **/

    private static $timestamps = false;

    /*
    It verifies if the relation has the softdelete, that is, if the records, when asked for their deletion, will in fact be erased, or just marked the date they were deleted, without being removed from the relation

     **/

    private static $softdelete = false;

    /*

    Parameter for relationship between database relationships "join"

     */

    private static $join = null;

    /*

    Parameter for ajust timezone database

     */

    private static $timezone = 'America/Fortaleza';  

    /*
    Imports models to use their methods and attributes
    @param $model: Model to import

     **/

    private static function importModel($model)
    {
        include_once $model . '.php';
    }

    /**
    Checks the name of the model for mapping the parameters of the relation registered in the database
     */

    private static function getModel()
    {return get_called_class();}

    /**
    Configures the parameters of the relation based on the information described in the model
     */

    private static function configureParametersRelation()
    {
        $model = self::getModel();
        self::$fillable = get_class_vars($model)['fillable'];
        self::$table = get_class_vars($model)['table'];
        self::$primary_key = isset(get_class_vars($model)['primaryKey']) ? get_class_vars($model)['primaryKey'] : 'id_';
        self::$timestamps = isset(get_class_vars($model)['timestamps']) ? get_class_vars($model)['timestamps'] : null;
        self::$softdelete = isset(get_class_vars($model)['softdelete']) ? get_class_vars($model)['softdelete'] : null;
    }

    /**
    Gets an instance of the database for access to relationships
     */

    private static function connectDB()
    {return Conexao::getInstance()->connect();}

    /**
    Disconnects from the database if there is an open connection.
     */

    private static function disconnectDB()
    {Conexao::getInstance()->close();}

    private static function executeCommand($sql)
    {
        date_default_timezone_set(self::$timezone);
        if (Conexao::getInstance()->typeDatabase() == 'mysql') {
            return mysqli_query(self::connectDB(), $sql);
        } else if (Conexao::getInstance()->typeDatabase() == 'postgresql') {
            return pg_query(self::connectDB(), $sql);
        } else {
            echo "Type database not found to connection!";
            exit;
        }
    }

    /**
    Structure a SQL for query of type SELECT

    @param $columns: filter the selection only for the desired relationship columns

    @param $extras: adds some extra command in sql. For example, an order by, among others.
     */

    private static function querySelect($columns = [], $extras = [])
    {
        self::configureParametersRelation();
        if (count($columns) == 0) {
            $columns = ['*'];
        }

        $sql = 'SELECT ';
        $flag = 0;
        foreach ($columns as $column) {
            $flag++;
            if ($flag + 1 <= count($columns)) {
                $sql .= $column . ', ';
            } else {
                $sql .= $column;
            }
        }
        $sql .= ' FROM ' . self::$table . ' ';
        if (self::$join != null) {
            $sql .= self::$join;
        }
        if (self::$softdelete) {
            $sql .= ' where ' . self::$table . '.deleted_at is null ';
        }
        foreach ($extras as $extra) {
            $sql .= $extra;
        }
        return $sql;
    }

    /**
    Returns all elements of a relation in object format
    @param $columns: filter the selection only for the desired relationship columns
    @param $extras: adds some extra command in sql. For example, an order by, among others.
     */

    public static function all($columns = [], $extras = [])
    {
        $sql = self::querySelect($columns = [], $extras = []);
        $result = self::executeCommand($sql);
        $data = [];
        if ($result) {
            if (Conexao::getInstance()->typeDatabase() == 'mysql') {
                while ($row = $result->fetch_object()) {
                    $data[] = $row;
                }
            } else if (Conexao::getInstance()->typeDatabase() == 'postgresql') {
                $result = $result->pg_fetch_object($result);
                $rowLine = 0;
                while ($row = $pg_fetch_object($result, $rowLine)) {
                    $data[] = $row;
                }
            }

        }
        self::disconnectDB();
        return $data;
    }

    /**
    Returns all elements of a relation in array format

    @param $columns: filter the selection only for the desired relationship columns

    @param $extras: adds some extra command in sql. For example, an order by, among others.
     */

    public static function get($columns = [], $extras = [])
    {
        $sql = self::querySelect($columns = [], $extras = []);
        $result = mysqli_query(self::connectDB(), $sql);
        $data = [];
        if ($result) {
            if (Conexao::getInstance()->typeDatabase() == 'mysql') {
                while ($row = $result->fetch_assoc()) {
                    $data[] = $row;
                }

            } else if (Conexao::getInstance()->typeDatabase() == 'postgresql') {
                while ($row = pg_fetch_assoc($result)) {
                    $data[] = $row;
                }
            }
        }
        self::disconnectDB();
        return $data;
    }

    /*
    Searching for an element through its primary key
    @param $id: value of the primary key of the record to be searched

     **/

    public static function find($id)
    {
        $sql = self::querySelect($columns = [], $extras = []);
        if (self::$primary_key == null) {
            return ['status' => false, 'msg' => 'Primary key not found for model ' . self::$table];
        }

        if (self::$softdelete) {
            $sql .= ' and ' . self::$primary_key . ' = ' . $id;
        } else {
            $sql .= ' where ' . self::$primary_key . ' = ' . $id;
        }
        $result = self::executeCommand($sql);
        $data = null;
        if ($result) {
            if (Conexao::getInstance()->typeDatabase() == 'mysql') {
                while ($row = $result->fetch_object()) {
                    $data = $row;
                }
            } else if (Conexao::getInstance()->typeDatabase() == 'postgresql') {
                $result = $result->pg_fetch_object($result);
                $rowLine = 0;
                while ($row = $pg_fetch_object($result, $rowLine)) {
                    $data = $row;
                }
            }
        }
        self::disconnectDB();
        return $data;
    }

    /*
    Create a new record in the relationship

    @param $values: Values to be inserted in the relation. REMINDER: It is not necessary to pass the value of the primary key, if it exists in the relation, as well as the values of created_at and updated_at, if timestamps is used, and softdeleted_at if used.

     **/

    public static function create($values = [])
    {
        if (!is_array($values)) {
            return ['status' => false, 'msg' => 'We expect to receive the array as parameter'];
        }
        try {
            self::configureParametersRelation();
            $sql = 'INSERT INTO ' . self::$table . ' ( ';
            $flag = 0;
            if (self::$primary_key != null) {
                $sql .= '' . self::$primary_key . ',';
            }
            $columns = self::$fillable;
            foreach ($columns as $column) {
                $flag++;
                if ($flag + 1 <= count($columns)) {
                    $sql .= $column . ', ';
                } else {
                    $sql .= $column;
                }
            }
            $timestamps = self::$timestamps;
            $softdelete = self::$softdelete;
            if ($timestamps != null && $timestamps) {
                $sql .= ',created_at,';
                $sql .= 'updated_at';
            }

            if ($softdelete != null && $softdelete) {
                $sql .= ',deleted_at';
            }
            $sql .= ") VALUES ( ";
            if (self::$primary_key) {
                $sql .= 'null,'; //Primary key auto_increment
            }
            $flag = 0;
            foreach ($values as $value) {
                $flag++;
                $value = is_string($value) ? iconv('UTF-8', 'ISO-8859-1//IGNORE', $value) : $value;
                if ($flag + 1 <= count($values)) {
                    $sql .= "'$value'" . ', ';
                } else {
                    $sql .= "'$value'";
                }
            }
            if ($timestamps != null && $timestamps) {
                $sql .= ",'" . date('Y-m-d H:i:s') . "',";
                $sql .= "'" . date('Y-m-d H:i:s') . "'";
            }
            if ($softdelete != null && $softdelete) {
                $sql .= ',null';
            }
            $sql .= ")";
            $result = self::executeCommand($sql);
            if ($result) {
                if (Conexao::getInstance()->typeDatabase() == 'mysql') {
                    $data = ['status' => true, 'id' => mysqli_insert_id(self::connectDB())];
                } else if (Conexao::getInstance()->typeDatabase() == 'postgresql') {
                    $data = ['status' => true, 'id' => pg_last_oid($result)];
                }

            } else {
                if (Conexao::getInstance()->typeDatabase() == 'mysql') {
                    $data = ['status' => false, 'msg' => mysqli_error(self::connectDB())];
                } else if (Conexao::getInstance()->typeDatabase() == 'postgresql') {
                    $data = ['status' => false, 'msg' => pg_result_error($result)];
                }

            }
            self::disconnectDB();
            return $data;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /*
    Removes a record of the relationship, taking into account whether or not the softdelete is enabled

    @param $id: single value or array of primary key ids of records to be deleted

     */

    public static function destroy($ids)
    {
        if (is_array($ids)) {
            $ids_destroy = $ids;
        } else {
            $ids_destroy[] = $ids;
        }

        if (count($ids) == 0) {
            return ['status' => false, 'msg' => 'Enter the records to be deleted!'];
        }

        $msgAlertReturn = ' NOTIFY -  the following ids have already been deleted: ';
        $record_already_deleted = false;
        $msg = '';
        foreach ($ids_destroy as $id) {
            $deleted = self::find($id);
            if ($deleted == null) {
                $msgAlertReturn .= $id . ' - ';
                $record_already_deleted = true;
                continue;
            } else {
                self::configureParametersRelation();
                try {
                    $softdelete = self::$softdelete;
                    if ($softdelete != null && $softdelete) {
                        $sql = 'UPDATE ' . self::$table . ' SET deleted_at =  ' . "'" . date('Y-m-d H:i:s') . "'" . ' WHERE ' . self::$primary_key . ' = ' . $id;
                    } else {
                        $sql = 'DELETE FROM ' . self::$table . ' WHERE ' . self::$primary_key . ' = ' . $id;
                    }
                    $result = self::executeCommand($sql);
                    self::disconnectDB();
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        if ($record_already_deleted) {
            $msg = 'Execution successfully! ' . $msgAlertReturn;
        } else {
            $msg = 'Execution successfully!';
        }
        return ['status' => true, 'msg' => $msg];

    }

     /*
    Debug parameter with var_dump

    @param $debug: parameter to debug

     */

    public function dd($debug){
        return var_dump($debug);
    }

    public function hasOne($object, $foreignKey, $localKey = null)
    {
        self::importModel($object);
        $nameRelation = get_class_vars($object)['table'];
        $key_local = $localKey == null ? self::$primary_key : $localKey;
        if (self::$join != null) {
            self::$join .= ' JOIN ' . $nameRelation . ' ON ' . self::$table . '.' . $key_local . ' = ' . $nameRelation . '.' . $foreignKey;
        } else {
            self::$join = ' JOIN ' . $nameRelation . ' ON ' . self::$table . '.' . $key_local . ' = ' . $nameRelation . '.' . $foreignKey;
        }

        return;
    }

    public function hasMany($object, $foreignKey = null, $localKey = null)
    {
        self::importModel($object);
        $nameRelation = get_class_vars($object)['table'];
        $key_local = $localKey == null ? self::$primary_key : $foreignKey;
        $foreignKey = $foreignKey == null ? self::$primary_key : $foreignKey;
        if (self::$join != null) {
            self::$join .= ' JOIN ' . $nameRelation . ' ON ' . self::$table . '.' . $key_local . ' = ' . $nameRelation . '.' . $foreignKey;
        } else {
            self::$join = ' JOIN ' . $nameRelation . ' ON ' . self::$table . '.' . $key_local . ' = ' . $nameRelation . '.' . $foreignKey;
        }

        return;
    }

    private static function verifyExistsWithMethod($method)
    {
        $class_methods = get_class_methods(self::getModel());
        $exists = false;
        foreach ($class_methods as $method_name) {
            if (strcmp(strtolower($method), strtolower($method_name)) == 0) {
                $exists = true;
            }
        }
        return $exists;
    }

    public static function with($methods = [])
    {
        if (count($methods) == 0) {
            echo 'State what relationships you want to do';
            exit;
        }
        self::configureParametersRelation();
        if (is_array($methods)) {
            $array_methods = $methods;
        } else {
            $array_methods[] = $methods;
        }

        foreach ($array_methods as $method) {
            if (self::verifyExistsWithMethod($method)) {
                $class = self::getModel();
                $reflectionMethod = new ReflectionMethod(self::getModel(), $method);
                $reflectionMethod->invoke(new $class);
            } else {
                echo 'Relation "' . $method . '" not found';
                exit;
            }
        }

        $class = self::getModel();
        return new $class;
    }
}
