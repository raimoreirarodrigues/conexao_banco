<?php

require_once('../src/Router.php');

define('__APP_ROOT__', dirname(__DIR__));
//require __APP_ROOT__ . '/vendor/autoload.php';
use Hero\Router;
$router = new Router();
$router->on('GET', '/', function () {
    return 'Page default';
})->post('/(\w+)/(\w+)/(\w+)', function ($module, $class, $method) {
    var_dump([$module, $class, $method]);
})->get('/view/(\w+)', function ($view) {
    ob_start();
    if(file_exists(dirname(__DIR__) . "/view/{$view}.php")){
    require dirname(__DIR__) . "/view/{$view}.php";
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
	}else{
		return "Page invalid - Error 404";
	}
})->get('/controller/(\w+)', function ($controller) {
    if(file_exists(dirname(__DIR__) . "/src/controller/{$controller}.php")){
    require dirname(__DIR__) . "/src/controller/{$controller}.php";
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}

})->get('/(.*)', function ($uri) {
    
});
echo $router($router->method(), $router->uri());